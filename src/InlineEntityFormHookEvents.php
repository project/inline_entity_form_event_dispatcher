<?php

namespace Drupal\inline_entity_form_event_dispatcher;

use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Defines events for Inline Entity Form hooks.
 */
final class InlineEntityFormHookEvents {

  /**
   * Perform alterations before a form is rendered.
   *
   * @Event
   *
   * @see \Drupal\inline_entity_form_event_dispatcher\Event\InlineEntityFormEntityFormAlterEvent
   * @see inline_entity_form_event_dispatcher_inline_entity_form_entity_form_alter()
   * @see hook_inline_entity_form_entity_form_alter()
   *
   * @var string
   */
  public const ENTITY_FORM_ALTER = HookEventDispatcherInterface::PREFIX . 'inline_entity_form.entity_form.alter';

}
