<?php

namespace Drupal\inline_entity_form_event_dispatcher\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\hook_event_dispatcher\Event\EventInterface;
use Drupal\inline_entity_form_event_dispatcher\InlineEntityFormHookEvents;

/**
 * Defines an event triggered when altering an Inline Entity Form.
 *
 * @HookEvent(
 *    id = "inline_entity_form_entity_form_alter",
 *    alter = "inline_entity_form_entity_form",
 *  )
 */
class InlineEntityFormEntityFormAlterEvent extends Event implements EventInterface {

  /**
   * The entity form.
   *
   * @var array
   */
  private $entityForm = [];

  /**
   * The form state.
   *
   * @var \Drupal\Core\Form\FormStateInterface
   */
  private $formState;

  /**
   * InlineEntityFormEntityFormAlterEvent constructor.
   *
   * @param array $entityForm
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The current state of the form. The arguments that
   *   \Drupal::formBuilder()->getForm() was originally called with are
   *   available in the array $form_state->getBuildInfo()['args'].
   */
  public function __construct(array &$entityForm, FormStateInterface $formState) {
    $this->entityForm = &$entityForm;
    $this->formState = $formState;
  }

  /**
   * Get the form.
   *
   * @return array
   *   The form.
   */
  public function &getEntityForm(): array {
    return $this->entityForm;
  }

  /**
   * Get the entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The form.
   */
  public function getEntity(): ?EntityInterface {
    return $this->entityForm['#entity'] ?? NULL;
  }

  /**
   * Get the form state.
   *
   * @return \Drupal\Core\Form\FormStateInterface
   *   The form state.
   */
  public function getFormState(): FormStateInterface {
    return $this->formState;
  }

  /**
   * {@inheritdoc}
   */
  public function getDispatcherType(): string {
    return InlineEntityFormHookEvents::ENTITY_FORM_ALTER;
  }

}
